import express from 'express';
import path from 'path';
import fs from 'fs';
import axios from 'axios';
import Loadable from 'react-loadable';
import configureStore from '../../client/configureStore';

import { 
  renderApp, 
  getAppPromises
} from '../utils/ssrHelpers';

const production = process.env.NODE_ENV == 'production' ? true : false;

const port = 8000;
const app = express();

const filename = path.resolve(__dirname, '..', '..', '..', 'dist', 'app.html');

let appHtml = '';
/* Make Sure HTML file exists */
try{
  appHtml = fs.readFileSync(filename, 'utf8');
}catch(err){
  throw new Error("HTML File was not found");
}
/* Make Static Directories */
app.use(express.static(path.resolve(__dirname, '..', '..', '..', 'dist')));
app.use('/dist', express.static(path.resolve(__dirname, '..', '..', '..', 'dist')));

/* Handle All Requests */
app.get('*', (req, res, next) => {
  try{
    const store = configureStore();
    let promises = getAppPromises(req, store);
    axios.all(promises).then(() => {
      res.send(renderApp(req, appHtml, store));
    }).catch((err) => {
      console.log(err);
      next(err);
    });
  }catch(err){
    next(err);
  }
});

/* Global Error Handler */
app.use((err, req, res, next) => {
  // console.log(err);
  res.status(500).send({
    err: err
  });
});

/* Start Express Server */

Loadable.preloadAll().then(() => {
  app.listen(port, () => {
    console.log(
      `
      =====================================================
      -> Production Server Running at port ${port}...
      =====================================================
    `
    );
  }).on('error', (err) => {
    console.log(err);
  });
}).catch((err) => {
  console.log('Unable to preload', err);
});
