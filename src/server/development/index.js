const webpack = require('webpack');
const path = require('path');
const devMiddleware = require('webpack-dev-middleware');
const hotMiddleware = require("webpack-hot-middleware");
const webpackConfig = require('../../../webpack.config');
const compiler = webpack(webpackConfig);
const express = require('express');
const app = express();
const port = 3000;


const devMiddlewareInstance = devMiddleware(compiler, {
  index: 'app.html'
});

app.use(devMiddlewareInstance);
app.use(hotMiddleware(compiler));
app.use('*', function(req, res){
  const path = webpackConfig.output.path + '/app.html';
  const html = devMiddlewareInstance.fileSystem.readFileSync(path);
  res.set('Content-Type', 'text/html');
  res.send(html);
});

app.listen(port, () => console.log("Example app listening on port "+ port +" !"))