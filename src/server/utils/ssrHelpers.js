import React from 'react';
import { renderToString } from 'react-dom/server';
import Helmet from 'react-helmet';
import { matchRoutes } from 'react-router-config';
import Loadable from 'react-loadable';
import { getBundles } from 'react-loadable/webpack';
import App from 'client/Server.jsx';
import routes from 'client/routes';
import stats from '../../../dist/react-loadable.json';

const renderApp = (req, htmlMarkup, store) => {
  let modules = [];
  const html = renderToString(
    <Loadable.Capture report={moduleName => modules.push(moduleName)}>
        <App store={store} req={req} />
    </Loadable.Capture>
  );
  let bundles = getBundles(stats, modules);

  let scripts = bundles.map(bundle => {
    if(bundle && bundle.file){
      return `<script src="/${bundle.file}"></script>`;
    }else{
      return ``;
    }
  }).join('\n');



  const helmet = Helmet.renderStatic();
  htmlMarkup = htmlMarkup.replace('<div id="root"></div>', '<div id="root">'+ html +'</div>');
  htmlMarkup = htmlMarkup.replace('<!-- metaTitle -->', helmet.title.toString());
  htmlMarkup = htmlMarkup.replace('<!-- metaTags -->', helmet.meta.toString());
  htmlMarkup = htmlMarkup.replace('</body>', scripts.toString() + '</body>');
  let storeString = '{}';
  try{
    storeString = JSON.stringify(store.getState()).replace(/</g, '\\u003c');
  }catch(e){
    storeString = '{}';
  }
  htmlMarkup = htmlMarkup.replace('window.__PRELOADED_STATE__ = {};', 'window.__PRELOADED_STATE__ = '+ storeString +';');
  return htmlMarkup;
};

const getAppPromises = (req, store) => {
  let promises = [];
  matchRoutes(routes, req.url).map(({route, match}) => {
    if(typeof route.fetchData == 'function'){
      let promise = route.fetchData(match, store.dispatch);
      if(promise.length){
        promises.push(...promise);
      }else{
        promises.push(promise);
      }
    }
  });
  return promises;
};

export {
  renderApp,
  getAppPromises
};