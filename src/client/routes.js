import Loadable from 'react-loadable';
import Loading from 'common/components/loading';
import { getGithubUsers } from 'client/home/home.actions';

const Home = Loadable({
  loader: () => import('client/home'),
  loading: Loading,
});

const About = Loadable({
  loader: () => import('client/about'),
  loading: Loading,
});

const routes = [
  {
    path: '/',
    exact: true,
    component: Home,
    fetchData: (match, dispatch) => {
      return [
        dispatch(getGithubUsers())
      ];
    }
  },
  {
    path: '/about',
    component: About
  }
];

export default routes;