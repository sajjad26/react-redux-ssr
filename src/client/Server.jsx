import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import routes from 'client/routes';

const context = {};

const App = ({ store, req }) => {
  return (
    <Provider store={store}>
      <StaticRouter context={context} location={req.url}>
        {renderRoutes(routes)} 
      </StaticRouter>
    </Provider>
  );
};

export default App;