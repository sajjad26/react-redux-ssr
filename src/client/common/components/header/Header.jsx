import React from 'react';
import { Link } from 'react-router-dom';

export default class Header extends React.PureComponent {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="navbar navbar-default">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/">Title</Link>
          <ul className="nav navbar-nav">
            <li className="active">
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
