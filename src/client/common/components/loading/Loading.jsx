import React from 'react';
import PropTypes from 'prop-types';

export default class Loading extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>Loading...</div>
    );
  }
}

Loading.propTypes = {
  loading: PropTypes.bool,
};

Loading.defaultProps = {
  loading: true
};