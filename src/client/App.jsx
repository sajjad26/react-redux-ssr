import React, { Fragment } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import PropTypes from 'prop-types';
import routes from 'client/routes';
import Header from 'common/components/header';

const App = ({ store }) => (
  <Provider store={store}>
    <BrowserRouter>
      <Fragment>
        <Header />
        {renderRoutes(routes)}
      </Fragment>
    </BrowserRouter>
  </Provider>
);

App.propTypes = {
  store: PropTypes.object,
};

export default App;
