import {combineReducers} from 'redux';
import home from './home/home.reducer';

const RootReducer = combineReducers({
  home
});

export default RootReducer;