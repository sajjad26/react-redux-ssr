import React from 'react';
import { hydrate, render } from 'react-dom';
import App from './App.jsx';
import configureStore from './configureStore';
import './app.scss';

let renderApp = hydrate;
if (module.hot) {
  module.hot.accept();
  renderApp = render;
}

const preloadedState = window.__PRELOADED_STATE__;
const store = configureStore(preloadedState);

renderApp(
  <App store={store} />, document.getElementById('root')
);