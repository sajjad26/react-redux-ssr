import React from 'react';

export default class About extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h2>About Us</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur asperiores veritatis id, magni tenetur vel, recusandae expedita alias sint adipisci tempore voluptatem, aut numquam, facere tempora! Quis suscipit at, quo.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum provident et fugit dignissimos quas laudantium nesciunt ipsam aut quidem architecto asperiores velit ipsum explicabo, saepe perferendis dolore non sequi mollitia.</p>
      </div>
    );
  }
}
