import React from 'react';
import { connect } from 'react-redux';
import About from './About.jsx';

const mapStateToProps = (store) => {
  return {
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(About);