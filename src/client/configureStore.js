import {createStore, applyMiddleware, compose} from 'redux';
import thunkMiddleware from 'redux-thunk';

import rootReducer from './rootReducer';

let middleware = [thunkMiddleware];

if (process.env.NODE_ENV !== 'production') {
  
}

const composeEnhancers = (module.hot && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__  : compose;

const configureStore = (predefinedState) => {
  return createStore(
    rootReducer,
    predefinedState,
    composeEnhancers(
      applyMiddleware(...middleware)
    )
  );
};

export default configureStore;
