import React from 'react';
import { connect } from 'react-redux';
import Home from './Home.jsx';
import {
  updateCounter,
  getGithubUsers
} from './home.actions';

const mapStateToProps = (store) => {
  return {
    counter: store.home.counter,
    users: store.home.users
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCounter: (counter) => dispatch(updateCounter(counter)),
    getGithubUsers: () => dispatch(getGithubUsers()) 
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Home);