import {
  SET_HOME_STORE
} from './home.actions';

const initialState = {
  counter: 0,
  users: {
    data: [],
    fetching: false,
    error: null
  }
};

const Reducer = (state = initialState, action) => {
  switch(action.type){
    case SET_HOME_STORE:
      return Object.assign({}, state, action.payload);

    default:
      return state;
  }
};

export default Reducer;