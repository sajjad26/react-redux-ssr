import React from 'react';
import PropTypes from 'prop-types';

export default class Home extends React.Component {

  constructor(props) {
    super(props);
    this.incrementCounter = this.incrementCounter.bind(this);
    this.decrementCounter = this.decrementCounter.bind(this);
  }

  componentDidMount(){
    const { users } = this.props;
    const { data } = users;
    if(!data || !data.length){
      this.getGithubUsers();
    }
  }

  getGithubUsers(){
    const { getGithubUsers } = this.props;
    getGithubUsers();
  }

  incrementCounter(){
    const { updateCounter, counter } = this.props;
    updateCounter(counter + 1);
  }

  decrementCounter(){
    const { updateCounter, counter } = this.props;
    updateCounter(counter - 1);
  }

  render() {
    const { counter, users } = this.props;
    const { data, fetching, error } = users;
    return (
      <div className="container">
        <div>
          <div className="row">
            <div className="col-sm-1">
              <button className="btn btn-primary btn-block" onClick={this.incrementCounter}>+</button>
            </div>
            <div className="col-sm-1">
              <button className="btn btn-primary btn-block">{counter}</button>
            </div>
            <div className="col-sm-1">
              <button className="btn btn-primary btn-block" onClick={this.decrementCounter}>-</button>
            </div>
          </div>
        </div>
        <div>
          <div>
            {(() => {
              if(fetching){
                return (
                  <div style={{ marginTop: '10px' }}>
                    Loading...
                  </div>
                );
              }else if(error){
                return (
                  <div>{error}</div>
                );
              }else{
                return (
                  <ul className="list-group">
                    {data.map((u, i) => {
                      return (
                        <li key={i} className="list-group-item">
                          <span>{u.login}</span>
                        </li>
                      );
                    })}
                  </ul>
                );
              }
            })()}
          </div>
        </div>
      </div>
    );
  }
}

Home.propTypes = {
  users: PropTypes.object,
  counter: PropTypes.number,
  getGithubUsers: PropTypes.func,
  updateCounter: PropTypes.func
};