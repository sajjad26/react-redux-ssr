import axios from 'axios';
export const SET_HOME_STORE = 'SET_HOME_STORE';

export const updateCounter = (counter = 1) => {
  return dispatch => {
    return dispatch(setHomeStore({
      counter: counter
    }));
  };
};

export const getGithubUsers = () => {
  return dispatch => {
    let url = `https://api.github.com/search/users`;
    let params = {
      q: `sajjad2`
    };
    dispatch(setHomeStore({
      users: {
        fetching: true
      }
    }));
    return axios.get(url, {
      params: params
    }).then((res) => {
      return res.data.items;
    }).then((users) => {
      return dispatch(setHomeStore({
        users: {
          data: users,
          fetching: false,
          error: null
        }
      }));
    }).catch((err) => {
      return dispatch(setHomeStore({
        user: {
          fetching: false,
          error: err.toString()
        }
      }));
    });
  };
};

export const setHomeStore = (payload) => {
  return {
    type: SET_HOME_STORE,
    payload: payload
  };
}