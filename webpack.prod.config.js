const webpack = require('webpack');
const path = require('path');
const Loadable = require('react-loadable/webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ReactLoadablePlugin = Loadable.ReactLoadablePlugin;

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './src/client/index.html',
  filename: 'app.html',
  inject: 'body'
});

module.exports = {
  devtool: "sourcemap",
  entry: {
    app: ['./src/client/index.js']
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: '[name].bundle.js'
  },
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.js|\.jsx$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: "style-loader" // creates style nodes from JS strings
          },
          {
            loader: "css-loader" // translates CSS into CommonJS
          },
          {
            loader: "sass-loader" // compiles Sass to CSS
          }
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
            loader: 'file-loader',
            // options: {
            //     name: '[name].[ext]',
            //     outputPath: 'fonts/'
            // }
        }]
      }
    ]
  },
  plugins: [
    HtmlWebpackPluginConfig,
    new ReactLoadablePlugin({
      filename: './dist/react-loadable.json',
    }),
  ],
  resolve: {
    alias: {
      client: path.resolve(__dirname, 'src/client'),
      common: path.resolve(__dirname, 'src/client/common')
    }
  }
};